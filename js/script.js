// Keys array holds all keys
let key = document.getElementsByClassName('key');
let keys = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '&nbsp', '&nbsp', '&nbsp', '&nbsp', '&nbsp', '&nbsp'];

// Deck of all the keys
const deck = document.getElementById('key-deck');

function shuffle(array) {
  let currentIndex = array.length, temporaryValue, randomIndex;

  while (currentIndex != 0) {
    randomIndex= Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
};

document.body.onload = startLogIn();

function startLogIn() {

  // Shuffle keys
  keys = shuffle(keys);
  console.log(keys);

  const container = document.createElement('div');
  container.setAttribute('class', 'tile-container');
  document.getElementById('keypad').appendChild(container);

  for (let i = 0; i < keys.length; i++) {
    let div = document.createElement('div');
    div.setAttribute('class', 'tile');
    div.setAttribute('id', `tile${i}`);
    div.setAttribute('value', keys[i]);
    div.setAttribute('onclick', 'addValue(this)');
    div.innerHTML = keys[i];

    container.appendChild(div);
  }

  document.getElementById('submit').disabled = true;
}

function addValue(e) {
  if (e.innerHTML === '&nbsp;') {
    return;
  }

  var input = document.querySelector('#input');

  var currentPassword = e.innerHTML;
  var newPassword = input.value + currentPassword;
  input.value = newPassword;

  var newPasswordLength = newPassword.length;

  if(newPasswordLength === 4) {
    // console.log(newPassword);

    // Enable submit button
    document.getElementById('submit').disabled = false;
    
    // Show password in console.log
    document.addEventListener("click", function() {
      console.log(newPassword);
    });
  }
}
